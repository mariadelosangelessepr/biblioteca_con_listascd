/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author DOCENTE
 */
public class Solicitud {
    
    private int codLibro;
    private byte tipoServicio;
    private int codEstudiante;
    
    public Solicitud(){
    }
    
    public Solicitud(int codLibro, byte tipoServicio, int codEstudiante){
        this.codLibro = codLibro;
        this.tipoServicio = tipoServicio;
        this.codEstudiante = codEstudiante;
    }
}
