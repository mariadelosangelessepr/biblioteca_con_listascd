/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template

Enunciado:
https://docs.google.com/document/d/1cAXPd_GRT8PyyP85mc52qqf2gRnbILl9/edit?usp=sharing&ouid=107614452311706692726&rtpof=true&sd=true
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Facultad;
import Modelo.Mensaje;
import Modelo.Solicitud;
import Util.seed.ListaCD;
import Util.seed.ArchivoLeerURL;

/**
 *
 * @author DOCENTE
 */
public class Biblioteca {
    
    private ListaCD<Facultad> facultades=new ListaCD();
    private ListaCD<Solicitud> solicitudes=new ListaCD();
    private ListaCD<Estudiante> estudiantes=new ListaCD();
    
    public Biblioteca() {
    }
    
    public void cargarSolicitudes(String urlSolicitudes)
    {
        ArchivoLeerURL solicitudesArchivo =new ArchivoLeerURL(urlSolicitudes);
        Object datos[]= solicitudesArchivo.leerArchivo();
        for(int i = 1; i < datos.length ; i++){
            String fila = datos[i].toString();
            String data[]=fila.split(",");
            int codLib = Integer.parseInt(data[0]);
            byte tipo = Byte.parseByte(data[1]);
            int codEst = Integer.parseInt(data[2]);
            this.solicitudes.insertarFinal(new Solicitud(codLib, tipo, codEst));

        }
    
    }
    
    
    public void cargarFacultades(String urlFactultades)
    {
        ArchivoLeerURL facultadesArchivo =new ArchivoLeerURL(urlFactultades);
        Object datos[]= facultadesArchivo.leerArchivo();
        for(int i = 1; i < datos.length ; i++){
            String fila = datos[i].toString();
            String data[]=fila.split(",");
            int codigo = Integer.parseInt(data[0]);
            String nombre = data[1];
            this.facultades.insertarFinal(new Facultad(codigo, nombre));

        }
    
    }
    
    public void cargarEstudiantes(String urlEstudiantes)
    {
        ArchivoLeerURL estudiantesArchivo =new ArchivoLeerURL(urlEstudiantes);
        Object datos[]= estudiantesArchivo.leerArchivo();
        for(int i = 1; i < datos.length ; i++){
            String fila = datos[i].toString();
            String data[]=fila.split(",");
            byte semestre = Byte.parseByte(data[0]);
            int codigo = Integer.parseInt(data[1]);
            String nombre = data[2];
            String correo = data[3];
            this.estudiantes.insertarFinal(new Estudiante(semestre, codigo, nombre, correo));

        }
    
    }
    
    public void cargarInventario(String urlInventario)
    {
        ArchivoLeerURL estudiantesArchivo =new ArchivoLeerURL(urlEstudiantes);
    
    }
    
    
    public ListaCD<Mensaje> procesarSolicitudes()
    {
        return null;
    }
    
    
    public void crearPDF(ListaCD<Mensaje> resultado)
    {
    
    }
    
    
}
